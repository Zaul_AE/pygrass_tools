#!/usr/bin/env python

"""
MODULE:    t.rast.in.pcraster

AUTHOR:
    Saul Arciniega-Esparza
    Hydrogeology Group, Faculty of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Export rasters from a temporal raster dataset to pcraster format files
    for Dynamic Framework purposes


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Import a temporal pcraster serie as a strds
#% keyword: strds
#% keyword: pcraster
#% keyword: import
#%end

#%option G_OPT_M_DIR
#% key: folder
#% label: Input folder where pcrasters are located
#% required: yes
#% multiple: no
#%end

#%option
#% key: pcbase
#% type: string
#% label: PCRaster basename (example: ETa)
#% required: yes
#%end

#%option
#% key: basename
#% type: string
#% label: Prefix for output rasters
#% required: yes
#%end

#%option G_OPT_STDS_INPUT
#% key: strds
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%option
#% key: date
#% type: string
#% label: Initial date
#% required: no
#% answer: 1980-01-01 00:00:00
#%end

#%option
#% key: step
#% type: string
#% label: Time step to generate the end_time of rasters (NNN seconds, minutes, hours, days, months, years)
#% required: no
#% answer: 1 days
#%end

#%option
#% key: startix
#% type: integer
#% label: Initial pcraster index
#% required: no
#% answer: 1
#%end

#%option
#% key: endix
#% type: integer
#% label: Final pcraster index
#% required: no
#% answer: 500
#%end

# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript

# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define subfunctions

def get_file_id():
    fileid = os.path.basename(fname)[-6:].replace(".", "")
    try:
        mapid = int(fileid)
    except:
        print("File extension must be numeric, but {fileid} was input")
        mapid = -1
    return mapid


def get_date(ix, step, start_date):
    return start_date + step * (ix - 1)


def import_raster(raster, saveas):
    gscript.run_command(
            "r.in.gdal",
            input=raster,
            output=saveas,
            overwrite=True,
            flags='o'
        )

def filter_files(folder, basename, index_range):

    list_files = []
    basename = basename.lower()

    for fname in os.listdir(folder):
        if fname.lower().startswith(basename):
            fileid = os.path.basename(fname)[-6:].replace(".", "")
            try:
                mapid = int(fileid)
            except:
                continue
            if mapid >= index_range[0] and mapid <= index_range[1]:
                list_files.append((mapid, fname))
    
    return list_files


def strds_exists(serie):
    """Verify if a strds already exist"""
    tlist = tgis.tlist('strds')
    if tlist:
        return serie in tlist
    else:
        return False


# Define main function

def main():
    # Read parameters
    options, flags = gscript.parser()
    # Parameters
    folder   = options['folder']
    pcbase   = options['pcbase']
    basename = options['basename']
    strds    = options['strds']
    date     = options['date']
    step     = options['step']
    startix  = options['startix']
    endix    = options['endix']

    mapset = gscript.gisenv()['MAPSET']

    fdate = pd.to_datetime(date)
    fstep = pd.Timedelta(step)

    # Connect with strds
    print('\nConnecting with raster time dataset')
    if '@' not in strds:
        strds += '@' + mapset

    if strds_exists(strds):
        stds = tgis.open_old_stds(
            strds,
            'strds'
        )
    else:
        stds = tgis.open_new_stds(
            strds,
            'strds',
            'absolute',
            strds.upper(),
            '.',
            'mean',
            overwrite=True
        )
    
    # Get file list
    file_list = filter_files(folder, pcbase, (startix, endix))
    if len(file_list) == 0:
        return 1
    
    maps_to_register = []
    for mapid, fname in file_list:
        # Files
        filename = os.path.join(folder, fname)
        sdate = get_date(mapid, fstep, fdate)
        edate = sdate + fstep
        time_str = sdate.strftime('%Y.%m.%d.%H.%M')
        saveas = '{}_{}@{}'.format(basename, time_str, mapset)
        # Import raster
        import_raster(filename, saveas)
        # create MapDataset
        map_dts = tgis.RasterDataset(saveas)
        map_dts.load()
        map_dts.set_absolute_time(
            start_time=sdate.to_pydatetime(),
            end_time=edate.to_pydatetime()
        )

        # Save raster list
        maps_to_register.append(map_dts)
    
    # Register maps in strds
    tgis.register.register_map_object_list(
        'raster',
        maps_to_register,
        stds
    )

    # Update dataset
    stds.update_from_registered_maps()
    stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())


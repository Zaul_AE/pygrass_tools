#!/usr/bin/env python

"""
MODULE:    t.rast.out.pcraster

AUTHOR:
    Saul Arciniega-Esparza
    Hydrogeology Group, Faculty of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Export rasters from a temporal raster dataset to pcraster format files
    for Dynamic Framework purposes


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Export strds as pcraster files
#% keyword: strds
#% keyword: pcraster
#% keyword: export
#%end

#%option G_OPT_STDS_INPUT
#% key: strds
#% description: Select a strds
#% required: yes
#% multiple: no
#%end

#%option G_OPT_M_DIR
#% key: folder
#% label: Output folder
#% required: yes
#%end

#%option
#% key: basename
#% type: string
#% label: Rasters basename (example: ETa)
#% required: yes
#%end

#%option
#% key: nodata
#% type: double
#% label: Nodata value
#% required: no
#% answer: -9999
#%end

#%option
#% key: startid
#% type: integer
#% label: Initial index for output map
#% required: no
#% answer: 1
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#% label: SQL query for strds
#%end


# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.raster import RasterRow

# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define sub-functions

def file_extension(x, var):
    n1 = len(var)
    n = 11 - n1
    b = ":0{}d".format(n)
    base = ("{" + b + "}").format(x)
    basename = var + base[:-3] + "." + base[-3:] + ".map"
    return basename

def write_pcraster(input_raster, output_file, nodata):
    gscript.run_command(
            "r.out.gdal",
            input=input_raster,
            output=output_file,
            format="PCRaster",
            type="Float32",
            nodata=nodata,
            createopt="PCRASTER_VALUESCALE=VS_SCALAR",
            overwrite=True
        )

def strds_info(strds, twhere):
    """
    Extracts rasters and time from a srtds as DataFrame
    """

    # Connect with time serie
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time",
        where=twhere,
        order="start_time"
    )

    rows = []
    for row in rasters:
        rows.append([row['start_time'], row['name'], row['mapset']])

    df = pd.DataFrame(
        rows,
        columns=['Date', 'Name', 'Mapset']
    )

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    return df

# Main function

def main():
    # Read parameters
    options, flags = gscript.parser()
    # required parameters
    strds = options['strds']
    folder = options['folder']
    out_basename = options['basename']

    # optionals
    twhere = options['twhere']
    nodata = options['nodata']
    startid = int(options['startid'])

    # Load strds elements
    df_strds = strds_info(strds, twhere)

    # Create folder if does not exists
    if not os.path.exists(folder):
        os.makedirs(folder)
    
    n = len(df_strds)
    for i in range(n):
        # raster_name@mapset
        raster = df_strds.iloc[i, 0] + "@" + df_strds.iloc[i, 1]
        # output filename
        file_basename = file_extension(i+startid, out_basename)
        filename = os.path.join(folder, file_basename)
        # white file
        write_pcraster(raster, filename, nodata)
    
    print("Finished...")
    return 0


if __name__ == '__main__':
    sys.exit(main())

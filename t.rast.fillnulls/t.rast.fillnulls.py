#!/usr/bin/env python

"""
MODULE:    t.rast.fillnulls

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Fill gaps in temporal rasters (strds) generating a new time dataset


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


#%module
#% description: Fillnulls for strds
#% keyword: strds
#% keyword: fillnulls
#%end

#%option G_OPT_STDS_INPUT
#% key: serie
#% description: Input raster time serie to fill
#% required: yes
#% multiple: no
#%end

#%option
#% key: basename
#% type: string
#% label: Prefix for output rasters
#% required: yes
#% answer: varname
#%end

#%option G_OPT_STDS_INPUT
#% key: saveas
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%flag
#% key: c
#% description: Use start_date to name rasters
#%end

#%option
#% key: method
#% type: string
#% options: bilinear,bicubic
#% label: Method to interpolate null values
#% required: no
#% answer: bilinear
#%end

#%option
#% key: fillval
#% type: integer
#% label: Fill constant value in case of empty raster
#% required: no
#% answer: 0
#%end

#%option
#% key: edge
#% type: integer
#% label: Width of edge used for interpolation (in cells)
#% required: no
#% answer: 3
#%end

#%option
#% key: npmin
#% type: integer
#% label: Minimum number of points for approximation in a segment
#% required: no
#% answer: 600
#%end

#%option
#% key: segmax
#% type: integer
#% label: Maximum number of points in a segment
#% required: no
#% answer: 300
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#%end


# Import modules
import sys
import numpy as np
import grass.script as gscript
from grass.pygrass.modules.shortcuts import raster as r
from grass.pygrass.vector import VectorTopo
from grass.pygrass.raster import RasterRow


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


def raster2numpy(rastname, mapset=''):
    """
    Return a numpy array from a raster map
    """
    with RasterRow(rastname, mapset=mapset, mode='r') as rast:
        return np.array(rast)


def has_null_cells(rastname, mapset):
    data = raster2numpy(rastname, mapset)
    r, c = data.shape
    nc = r * c
    number_nulls = np.sum(np.isnan(data))
    if number_nulls == nc:
        return 2
    elif number_nulls > 0:
        return 1
    else:
        return 0


def split_parts(layer):
    """
    Split a filename into name and mapset
    If mapset is not detected, current mapset is returned
    """
    if '@' in layer:
        name, mapset = layer.split('@')
    else:
        name = str(layer)
        mapset = gscript.gisenv()['MAPSET']
    return name, mapset


def strds_exists(serie):
    """Verify if a strds already exist"""
    tlist = tgis.tlist('strds')
    if tlist:
        return serie in tlist
    else:
        return False


def main():

    options, flags = gscript.parser()

    strds = options['serie']
    saveas = options['saveas']
    basename = options['basename']  # pedir texto
    method = options['method']
    fill_val = options['fillval']
    edge = options['edge']
    npmin = options['npmin']
    segmax = options['segmax']
    twhere = options['twhere']
    use_index = flags['c']

    mapset = gscript.gisenv()['MAPSET']

    # Connect with strds
    print('\nConnecting with strds...')
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time,end_time",
        where=twhere,
        order="start_time"
    )

    nmaps = len(rasters)

    if nmaps > 0:
        # Connect with new strds
        if '@' not in saveas:
            saveas += '@' + mapset
        
        if strds_exists(saveas):
            stds = tgis.open_old_stds(
                saveas,
                'strds'
            )
        else:
            stds = tgis.open_new_stds(
                saveas,
                'strds',
                'absolute',
                saveas.upper(),
                '.',
                'mean',
                overwrite=True
            )

        for i, raster in enumerate(rasters):

            st = raster['start_time']
            et = raster['end_time']
            input_file = raster['name'] + '@' + raster['mapset']

            if use_index:
                baseout = basename + '_' + str(st)
                output_file = basename + '_' + str(st) + '@' + mapset
            else:
                baseout = basename + '_' + str(i+1)
                output_file = basename + '_' + str(i+1) + '@' + mapset

            # Close gaps
            hnc = has_null_cells(raster['name'], raster['mapset']) 
            if hnc == 1:
                gscript.run_command(
                    'r.fillnulls',
                    input=input_file,
                    output=output_file,
                    method=method,
                    #lambda=lam,
                    edge=edge,
                    npmin=npmin,
                    segmax=segmax,
                    overwrite=True
                )
            elif hnc == 0:
                r.mapcalc(
                    '{} = {}'.format(baseout, input_file),
                    overwrite=True
                )
            elif hnc == 2:
                r.mapcalc(
                    '{} = {}'.format(baseout, fill_val),
                    overwrite=True
                )

            # create MapDataset
            map_dts = tgis.RasterDataset(output_file)
            map_dts.load()
            map_dts.set_absolute_time(
                start_time=st,
                end_time=et
            )
        
            # Register maps in strds
            tgis.register.register_map_object_list(
                'raster',
                [map_dts],
                stds
            )
        
        # Update dataset
        stds.update_from_registered_maps()
        stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())

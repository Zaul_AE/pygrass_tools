#!/usr/bin/env python

"""
MODULE:    t.rast.basins_stats

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Compute univariate statistic of a strds for each basin in a vector file
    It is similar to t.rast.vstats, but for small regions is faster


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Compute univariate statistic of a strds using polygons extent
#% keyword: strds
#% keyword: basins
#% keyword: statistics
#%end

#%option G_OPT_STDS_INPUT
#% key: serie
#% description: Name of input time raster dataset
#% required: yes
#% multiple: no
#%end

#%option G_OPT_R_INPUT
#% key: basins
#% description: Name of input basins raster layer
#% required: Yes
#% multiple: no
#%end

#%option
#% key: stat
#% type: string
#% label: Spatial aggregation at polygon extent
#% options: mean, min, max, sum, std, count
#% required: yes
#% answer: mean
#%end

#%option G_OPT_F_OUTPUT
#% key: output
#% label: Name of the output file
#% required: yes
#%end

#%option
#% key: separator
#% type: string
#% label: Column separator
#% options: comma, tab
#% required: yes
#% answer: comma
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#%end


# Import modules
import sys

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.vector import VectorTopo
from grass.pygrass.raster import RasterRow


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define separators
DELIMITERS = {
    'comma': ',',
    'tab': '\t'
}

FUN = {
    'mean': np.nanmean,
    'min': np.nanmin,
    'max': np.nanmax,
    'sum': np.nansum,
    'std': np.nanstd,
    'count': lambda x: np.sum(np.isfinite(x))
}


# Define sub-functions

def split_parts(layer):
    """
    Split a filename into name and mapset
    If mapset is not detected, current mapset is returned
    """
    if '@' in layer:
        name, mapset = layer.split('@')
    else:
        name = str(layer)
        mapset = gscript.gisenv()['MAPSET']
    return name, mapset


def raster2numpy(rastname, mapset=''):
    """
    Return a numpy array from a raster map
    """
    with RasterRow(rastname, mapset=mapset, mode='r') as rast:
        return np.array(rast)


def read_attribute_table(layer, mapset=''):
    """
    Returns the attribute table of a vector layer as a DataFrame
    """
    # Connect with layer
    layer = VectorTopo(layer, mapset=mapset)
    layer.open('r')

    # Read attribute table
    link = layer.dblinks[0]
    table = link.table()
    colnames = table.columns.names()
    rows = [row for row in table]

    # Close connection
    layer.close()

    # Create data frame
    df = pd.DataFrame(rows, columns=colnames)
    return df


def basins_id(layer, mapset):
    # Extracts the cells positions of each class

    grid = raster2numpy(layer + '@' + mapset)
    unique_values = np.unique(
        grid[np.isfinite(grid) & (grid >= 0)]
    ).astype(int)

    # Find basins classes positions
    class_values = dict.fromkeys(unique_values)
    for key in unique_values:
        class_values[key] = np.where(grid == key)

    return class_values


# Define main function

def main():
    # Load parameters
    options, flags = gscript.parser()
    strds = options['serie']
    basins = options['basins']
    stat = options['stat']
    output = options['output']
    sep = options['separator']
    twhere = options['twhere']

    print('\n\nComputing statistics for strds over polygons...')
    print('   strds: {}'.format(strds))
    print('  basins: {}\n'.format(basins))

    # Split file names
    basins_name, basins_mapset = split_parts(basins)

    # Extract unique values and data position
    class_values = basins_id(
        basins_name,
        basins_mapset
    )
    nvalues = len(class_values)

    # Connect with strds
    print('\nConnecting with strds...')
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time",
        where=twhere,
        order="start_time"
    )

    nmaps = len(rasters)

    # Create variables
    print('\nComputing statistics for each basin...')
    dates = np.full(nmaps, np.nan, dtype=object)
    data = np.full((nmaps, nvalues), np.nan, dtype=np.float32)

    # Iterate over basins

    for i, raster in enumerate(rasters):
        dates[i] = raster['start_time']

        # Read raster data as array
        layer = raster2numpy(raster['name'], raster['mapset'])

        # Compute aggregation for each class
        for j, key in enumerate(class_values):
            data[i, j] = FUN[stat](layer[class_values[key]])

    # Create a DataFrame
    print('\nCreating DataFrame...')
    df = pd.DataFrame(
        data,
        columns=class_values.keys(),
        index=dates
    )
    df.index.name = 'Dates'

    # Save file
    print('\nCreating delimited file {}...'.format(output))
    df.to_csv(output, sep=DELIMITERS[sep])


if __name__ == '__main__':
    sys.exit(main())

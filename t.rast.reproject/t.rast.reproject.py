#!/usr/bin/env python

"""
MODULE:    t.rast.reproject

AUTHOR:
    Saul Arciniega-Esparza
    Hydrogeology Group, Faculty of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Re-projects a strds from a file list given location and mapset to the current location


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Re-projects a strds from a given location to the current location
#% keyword: strds
#% keyword: pcraster
#% keyword: reproject
#%end

#%option G_OPT_M_LOCATION
#% key: location
#% description: Location containing the input strds
#% required: yes
#%end

#%option
#% key: mapset
#% description: Mapset containing the input strds
#% required: yes
#%end

#%option G_OPT_F_INPUT
#% key: file
#% label: Input file list with [name, start_date, end_date]
#% required: yes
#% multiple: no
#%end

#%option
#% key: delimiter
#% type: string
#% options: comma, tab, pipe
#% label: Input file separator
#% required: yes
#% answer: comma
#%end

#%option G_OPT_STDS_OUTPUT
#% key: strds
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%option
#% key: method
#% type: string
#% options: nearest, bilinear, bicubuc, lanczos, bilinear_f, bicubic_f, lanczos_f
#% label: Calendar used for time calculations (only if not defined as attribute)
#% required: no
#% answer: bilinear_f
#%end


# Import modules
import sys
import os

import pandas as pd

import grass.script as gscript

# Initialize the temporal library
import grass.temporal as tgis
tgis.init()

separators = {
    "comma": ",",
    "tab": "\n",
    "pipe": "|"
}


# Define sub-functions

def read_file_list(filename, sep):
    flist = pd.read_csv(filename, sep=separators[sep])
    flist.columns = [str(col).lower() for col in flist.columns]
    flist.loc[:, ["name", "start_time", "end_time"]]
    flist.loc[:, "start_time"] = pd.to_datetime(flist.loc[:, "start_time"])
    flist.loc[:, "end_time"] = pd.to_datetime(flist.loc[:, "end_time"])
    return flist

def strds_exists(serie):
    """Verify if a strds already exist"""
    tlist = tgis.tlist('strds')
    if tlist:
        return serie in tlist
    else:
        return False


# Define main function

def main():
    # Read parameters
    options, flags = gscript.parser()
    # Parameters
    location  = options["location"]
    lmapset   = options["mapset"]
    filename  = options["file"]
    separator = options["delimiter"]
    strds     = options["strds"]
    method    = options["method"]

    # current mapset
    mapset = gscript.gisenv()['MAPSET']
    # Load file list
    file_list = read_file_list(filename, separator)
    # Connect with strds
    print('\nConnecting with raster time dataset')
    if '@' not in strds:
        strds += '@' + mapset

    if strds_exists(strds):
        stds = tgis.open_old_stds(
            strds,
            'strds'
        )
    else:
        stds = tgis.open_new_stds(
            strds,
            'strds',
            'absolute',
            strds.upper(),
            '.',
            'mean',
            overwrite=True
        )
    
    maps_to_register = []
    for row in range(file_list.shape[0]):
        fname = file_list.loc[row, "name"]
        sdate = file_list.loc[row, "start_time"]
        edate = file_list.loc[row, "end_time"]
        saveas = fname + "@" + mapset

        gscript.run_command(
            "r.proj",
            location=location,
            mapset=lmapset,
            input=fname,
            output=saveas,
            method=method,
            overwrite=True,
            verbose=False
        )

        # create MapDataset
        map_dts = tgis.RasterDataset(saveas)
        map_dts.load()
        map_dts.set_absolute_time(
            start_time=sdate.to_pydatetime(),
            end_time=edate.to_pydatetime()
        )

        # Save raster list
        maps_to_register.append(map_dts)
    
    # Register maps in strds
    tgis.register.register_map_object_list(
        'raster',
        maps_to_register,
        stds
    )

    # Update dataset
    stds.update_from_registered_maps()
    stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())


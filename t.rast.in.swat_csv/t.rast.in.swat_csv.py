#!/usr/bin/env python

"""
MODULE:    t.rast.in.swat_csv

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Imports the csv climate data from the National Centers for Environmental Prediction (NCEP)
    Climate Forecast System Reanalysis (CFSR) used as forcings for SWAT model.
    The website allows you to download daily CFSR data (precipitation, wind, relative humidity,
    and solar) in csv format.
    
    https://globalweather.tamu.edu/


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Imports csv forcing data for SWAT model as strds
#% keyword: strds
#% keyword: import
#% keyword: forcings
#%end

#%option G_OPT_M_DIR
#% key: folder
#% description: Input folder where forcing csv files are stored
#%end

#%option
#% key: varname
#% type: string
#% label: Variable to import
#% options: Precipitation, Max Temperature, Min Temperature, Wind, Relative Humidity, Solar
#% required: yes
#% answer: Precipitation
#%end

#%option
#% key: basename
#% type: string
#% label: Prefix for output rasters
#% required: yes
#% answer: forcings
#%end

#%option G_OPT_STDS_INPUT
#% key: strds
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%option
#% key: start
#% type: string
#% label: Initial date in format yyyy-mm-dd
#% required: no
#%end

#%option
#% key: end
#% type: string
#% label: Final date in format yyyy-mm-dd
#% required: no
#%end


# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.raster import RasterRow
from grass.pygrass.raster.buffer import Buffer


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Global variables
VARDESCRIPTION = {
    'Precipitation': {'title': 'Precipitation Forcing from Climate Forecast System Reanalysis ',
                    'description': 'Precipitation in mm'},
    'Max Temperature': {'title': 'Tmax Forcing from Climate Forecast System Reanalysis ',
                        'description': 'Maximum Temperature in degrees'},
    'Min Temperature': {'title': 'Tmin Forcing from Climate Forecast System Reanalysis ',
                        'description': 'Minimum Temperature in degrees'},
    'Wind': {'title': 'Wind Forcing from Climate Forecast System Reanalysis ',
             'description': 'Wind velocity in m/s'},
    'Relative Humidity': {'title': 'Relative Humidity Forcing from Climate Forecast System Reanalysis ',
                          'description': 'Relative Humidity as fraction'},
    'Solar': {'title': 'Solar Forcing from Climate Forecast System Reanalysis ',
              'description': 'Solar radiation in MJ/m2'}
}

dxy = 0.3125  # longitude and latitude step derived from a forcing GRID


# Define functions

def get_data_from_weatherdata(folder, varname):
    """Returns the points ['Id', 'Station', 'Lon', 'Lat'] and variable time series as DataFrames"""

    coors = []
    data_series = pd.DataFrame([])
    cnt = -1
    for fname in os.listdir(folder):
        if fname.lower().endswith('.csv'):
            filename = os.path.join(folder, fname)
            df = pd.read_csv(filename, sep=',', index_col=False,
                             parse_dates=[0], engine='python')
            
            if varname not in df.columns:
                    raise ValueError('Variable {} was not found in one or more csv files'
                                     .format(varname))

            df.set_index('Date', inplace=True)
            
            # Get coordinates
            cnt += 1
            coors.append([
                cnt,
                os.path.splitext(fname)[0],
                df.iloc[0]['Longitude'],
                df.iloc[0]['Latitude']
                ]
            )

            # Get variable
            data_series[cnt] = df[varname]
    
    if len(coors) == 0:
        return pd.DataFrame([]), pd.DataFrame([])
    else:
        points = pd.DataFrame(coors, columns=['Id', 'Station', 'Lon', 'Lat'])
        return points, data_series


def get_lonlat_arrays(points):
    """
    Creates the lon and lat arrays that covers the extent of input points
    and assoc the points to a row and col of the coordinates matrix
    """

    # Get longitude and latitude arrays
    lon_min, lon_max = points['Lon'].min(), points['Lon'].max()
    lat_min, lat_max = points['Lat'].min(), points['Lat'].max()

    lon = np.arange(lon_min, lon_max + dxy / 2.0, dxy)
    lat = np.flipud(np.arange(lat_min, lat_max + dxy / 2.0, dxy))

    # Assoc series using row and col indexes
    indexes = pd.DataFrame(np.full((points.shape[0], 2), -1, dtype=int),
                           columns=['row', 'col'],
                           index=points.index)

    for i in range(points.shape[0]):
        lon_diff = np.abs(lon - points.iloc[i]['Lon'])
        lat_diff = np.abs(lat - points.iloc[i]['Lat'])
        
        row = np.argmin(lat_diff)
        col = np.argmin(lon_diff)
        dist = (lon_diff[col] ** 2.0 + lat_diff[row] ** 2.0) ** 0.5

        if dist < dxy / 10.0:
            indexes.iloc[i, :] = [row, col]
    
    return indexes, lon, lat


def create_data_array(serie, points, nrows, ncols):
    """Sort data from gauges into a 2D array"""
    data = np.full((nrows, ncols), np.nan)
    data[points['row'].values, points['col']] = serie[points['Id'].values]
    return data


def set_region(lon, lat):
    """Set computational region from longitude and latitude arrays"""
    nsres = np.abs(np.diff(lat)).mean()
    ewres = np.abs(np.diff(lon)).mean()

    cols = len(lon)
    rows = len(lat)

    south, north = lat.min() - nsres / 2., lat.max() + nsres / 2.
    west, east = lon.min() - ewres / 2., lon.max() + ewres/2.

    gscript.run_command(
        'g.region',
        n=north,
        s=south,
        e=east,
        w=west,
        rows=rows,
        cols=cols
    )


def array2raster(array, mtype, rastname, overwrite=False):
    """Save a numpy array to a raster map"""
    with RasterRow(rastname, mode='w', mtype=mtype, overwrite=overwrite) as new:
        newrow = Buffer((array.shape[1],), mtype=mtype)
        for row in array:
            newrow[:] = row[:]
            new.put_row(newrow)


def strds_exists(serie):
    """Verify if a strds already exist"""
    tlist = tgis.tlist('strds')
    if tlist:
        return serie in tlist
    else:
        return False


def main():
    # Read inputs
    options, flags = gscript.parser()

    folder = options['folder']
    varname = options['varname']
    basename = options['basename']
    strds = options['strds']

    init_date = options['start']
    final_date = options['end']
    if init_date:
        init_date = pd.Timestamp(init_date)
    else:
        init_date = None
    if final_date:
        final_date = pd.Timestamp(final_date)
    else:
        final_date = None

    mapset = gscript.gisenv()['MAPSET']

    # Check variable
    if varname not in VARDESCRIPTION:
        raise ValueError('Variable <{}> is not allowed'.format(varname)) 


    # Connect with strds
    print('\nConnecting with raster time dataset')
    if '@' not in strds:
        strds += '@' + mapset

    if strds_exists(strds):
        stds = tgis.open_old_stds(
            strds,
            'strds'
        )
    else:
        stds = tgis.open_new_stds(
            strds,
            'strds',
            'absolute',
            VARDESCRIPTION[varname]['title'],
            VARDESCRIPTION[varname]['description'],
            'mean',
            overwrite=True
        )

    # Read Forcing database
    print('\nReading forcings database...')
    points, series = get_data_from_weatherdata(folder, varname)

    # Filter time series
    print('Filtering time series...')
    if init_date is None:
        init_date = series.index.min()
    if final_date is None:
        final_date = series.index.max()
    
    series = series[init_date:final_date]

    # Get rows, cols and coordinates
    print('Getting coordinates...')
    indexes, lon, lat = get_lonlat_arrays(points)
    points['row'] = indexes['row']
    points['col'] = indexes['col']
    
    # Filter points
    points = points[(points['row'] >= 0) & (points['col'] >= 0)]

    # Set region
    print('Setting computational region...')
    set_region(lon, lat)
    nrows, ncols = len(lat), len(lon)

    # Iteration over dates
    print('Creating time serie...')
    ne = series.shape[0]
    maps_to_register = []
    for i in range(ne):
        # Create data and name
        time_str = series.index[i].strftime('%Y.%m.%d')
        raster_name = '{}.{}@{}'.format(basename, time_str, mapset)

        data = create_data_array(
            series.iloc[i],
            points,
            nrows,
            ncols
        )

        # Write raster
        array2raster(data, 'FCELL', raster_name, overwrite=True)

        # create MapDataset
        map_dts = tgis.RasterDataset(raster_name)
        map_dts.load()
        map_dts.set_absolute_time(
            start_time=series.index[i].to_pydatetime(),
            end_time=series.index[i].to_pydatetime() + pd.Timedelta('1d')
        )

        # Save raster list
        maps_to_register.append(map_dts)
    
    # Register maps in strds
    tgis.register.register_map_object_list(
        'raster',
        maps_to_register,
        stds
    )

    # Update dataset
    stds.update_from_registered_maps()
    stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())


#!/usr/bin/env python

"""
MODULE:    t.rast.vstats

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Exports a strds to a netcdf file using netCDF4


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Print netcdf properties
#% keyword: netcdf
#% keyword: info
#%end

#%option G_OPT_F_INPUT
#% key: filename
#% label: Name of the input netcdf file
#% required: yes
#% multiple: no
#%end

#%flag
#% key: a
#% description: Print only dimensions
#%end

#%flag
#% key: b
#% description: Print only variables
#%end


# Import modules
import sys
from pprint import pprint

import grass.script as gscript

from netCDF4 import Dataset


# Define main function
def main():
    # Read parameters
    options, flags = gscript.parser()
    filename = options['filename']

    if not(filename.lower().endswith('.nc') or filename.lower().endswith('.nc4')):
        raise IOError('File must have .nc or .nc4 extension!')

    with Dataset(filename) as fid:
        if flags['a']:
            print('\n\n ***************** DIMENSIONS ************************')
            pprint(fid.dimensions.keys())
        elif flags['b']:
            print('\n\n ****************** VARIABLES ************************')
            pprint(fid.variables.keys())
        else:
            print('\n\n ************* GENERAL INFORMATION *******************')
            pprint(fid)
            print('\n\n ***************** DIMENSIONS ************************')
            pprint(fid.dimensions)
            print('\n\n ****************** VARIABLES ************************')
            pprint(fid.variables)


if __name__ == '__main__':
    sys.exit(main())

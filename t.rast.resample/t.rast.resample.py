#!/usr/bin/env python

"""
MODULE:    t.rast.resample

AUTHOR:
    Saul Arciniega-Esparza
    Hydrogeology Group, Faculty of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Resample a strds in the current mapset


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Resample a strds using an interpolation method
#% keyword: strds
#% keyword: resample
#%end

#%option G_OPT_STDS_INPUT
#% key: istrds
#% description: Select a strds
#% required: yes
#% multiple: no
#%end

#%option
#% key: basename
#% type: string
#% label: Output rasters basename
#% required: yes
#%end

#%option G_OPT_STDS_OUTPUT
#% key: ostrds
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%option
#% key: method
#% type: string
#% options: nearest, bilinear, bicubic
#% label: Interpolation method
#% required: no
#% answer: bilinear
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#% label: SQL query for strds
#%end


# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript

# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define subfunctions 

def strds_info(strds, twhere):
    """
    Extracts rasters and time from a srtds as DataFrame
    """

    # Connect with time serie
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time,end_time",
        where=twhere,
        order="start_time"
    )

    rows = []
    for row in rasters:
        rows.append([row['start_time'], row['name'], row['mapset'], row['end_time']])

    df = pd.DataFrame(
        rows,
        columns=['Date', 'Name', 'Mapset', 'EndDate']
    )

    df['Date'] = pd.to_datetime(df['Date'])
    df['EndDate'] = pd.to_datetime(df['EndDate'])
    df.set_index('Date', inplace=True)

    return df


# Define main functions

def main():
    # Read parameters
    options, flags = gscript.parser()
    # required parameters
    istrds = options['istrds']
    basename = options['basename']
    ostrds = options['ostrds']
    # optionals
    twhere = options['twhere']
    method = options['method']

    mapset = gscript.gisenv()['MAPSET']

    # Load strds elements
    df_strds = strds_info(istrds, twhere)

    # Connect with strds
    print('\nConnecting with raster time dataset')
    if '@' not in ostrds:
        ostrds += '@' + mapset

    if strds_exists(ostrds):
        stds = tgis.open_old_stds(
            ostrds,
            'strds'
        )
    else:
        stds = tgis.open_new_stds(
            ostrds,
            'strds',
            'absolute',
            ostrds.upper(),
            '.',
            'mean',
            overwrite=True
        )

    maps_to_register = []
    n = len(df_strds)
    for i in range(n):
        # raster_name@mapset
        raster = df_strds.iloc[i, 0] + "@" + df_strds.iloc[i, 1]
        sdate  = df_strds.index.values[i]
        edate  = df_strds.iloc[i, 2]

        # output filename
        saveas = "{}_{}@{}".format(basename, sdate.strftime('%Y.%m.%d.%H.%M'), mapset)

        gscript.run_command(
            "r.resamp.interp",
            input=raster,
            output=saveas,
            method=method,
            overwrite=True,
        )

        # create MapDataset
        map_dts = tgis.RasterDataset(saveas)
        map_dts.load()
        map_dts.set_absolute_time(
            start_time=sdate.to_pydatetime(),
            end_time=edate.to_pydatetime()
        )

        # Save raster list
        maps_to_register.append(map_dts)
    
    # Register maps in strds
    tgis.register.register_map_object_list(
        'raster',
        maps_to_register,
        stds
    )

    # Update dataset
    stds.update_from_registered_maps()
    stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())


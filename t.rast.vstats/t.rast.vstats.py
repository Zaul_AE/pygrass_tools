#!/usr/bin/env python

"""
MODULE:    t.rast.vstats

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Compute univariate statistic of a strds for each polygon in a vector file


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Compute univariate statistic of a strds using polygons extent
#% keyword: strds
#% keyword: vector
#% keyword: statistics
#%end

#%option G_OPT_STDS_INPUT
#% key: serie
#% description: Name of input time raster dataset
#% required: yes
#% multiple: no
#%end

#%option G_OPT_V_INPUT
#% description: Name of input polygons vector layer
#% required: yes
#% multiple: no
#%end

#%option G_OPT_DB_COLUMN
#% key: column
#% description: Name of attribute to use as column name
#% required: yes
#%end

#%option
#% key: stat
#% type: string
#% label: Spatial aggregation at polygon extent
#% options: mean, min, max, sum, std, count
#% required: yes
#% answer: mean
#%end

#%option G_OPT_F_OUTPUT
#% key: output
#% label: Name of the output file
#% required: yes
#%end

#%option
#% key: separator
#% type: string
#% label: Column separator
#% options: comma, tab
#% required: yes
#% answer: comma
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#%end


# Import modules
import sys

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.vector import VectorTopo
from grass.pygrass.raster import RasterRow


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define separators
DELIMITERS = {
    'comma': ',',
    'tab': '\t'
}

FUN = {
    'mean': np.nanmean,
    'min': np.nanmin,
    'max': np.nanmax,
    'sum': np.nansum,
    'std': np.nanstd,
    'count': lambda x: np.sum(np.isfinite(x))
}

POLY_MASK = 'zone_mask'
RAST_MASK = 'mask_raster'


# Define sub-functions

def split_parts(layer):
    """
    Split a filename into name and mapset
    If mapset is not detected, current mapset is returned
    """
    if '@' in layer:
        name, mapset = layer.split('@')
    else:
        name = str(layer)
        mapset = gscript.gisenv()['MAPSET']
    return name, mapset


def raster2numpy(rastname, mapset=''):
    """
    Return a numpy array from a raster map
    """
    with RasterRow(rastname, mapset=mapset, mode='r') as rast:
        return np.array(rast)


def read_attribute_table(layer, mapset=''):
    """
    Returns the attribute table of a vector layer as a DataFrame
    """
    # Connect with layer
    layer = VectorTopo(layer, mapset=mapset)
    layer.open('r')

    # Read attribute table
    link = layer.dblinks[0]
    table = link.table()
    colnames = table.columns.names()
    rows = [row for row in table]

    # Close connection
    layer.close()

    # Create data frame
    df = pd.DataFrame(rows, columns=colnames)
    return df


def define_region(layer, mapset='', cat='1'):

    # Extract polygon
    gscript.run_command(
        'v.extract',
        input=layer + '@' + mapset,
        output=POLY_MASK,
        cats=str(cat),
        overwrite=True,
        quiet=True
    )
    # Set region using polygon
    gscript.run_command('g.region', vector=POLY_MASK, quiet=True)
    # Convert polygon to raster
    gscript.run_command(
        'v.to.rast',
        input=POLY_MASK,
        output=RAST_MASK,
        type='area',
        use='val',
        overwrite=True,
        quiet=True
    )


# Define main function

def main():
    # Load parameters
    options, flags = gscript.parser()
    strds = options['serie']
    polygons = options['input']
    field = options['column']
    stat = options['stat']
    output = options['output']
    sep = options['separator']
    twhere = options['twhere']

    print('\n\nComputing statistics for strds over polygons...')
    print('   strds: {}'.format(strds))
    print('polygons: {}'.format(polygons))

    # Define initial region
    gscript.run_command('g.region', vector=polygons, quiet=True)

    # Split file names
    poly_name, poly_mapset = split_parts(polygons)

    # Get vector attribute table
    print('\n\nReading attribute table...')
    attributes = read_attribute_table(poly_name, poly_mapset)
    attributes.set_index('cat', drop=False, inplace=True)
    attributes = attributes.loc[:, field]
    npoly = attributes.shape[0]

    # Connect with strds
    print('Connecting with strds...')
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time",
        where=twhere,
        order="start_time"
    )

    nmaps = len(rasters)

    # Create variables
    print('\n\nCreating output variables...')
    dates = np.full(nmaps, np.nan, dtype=object)
    data  = np.full((nmaps, npoly), np.nan, dtype=np.float32)

    # Iterate over polygons
    for i in range(npoly):
        # Set polygon as region and mask
        define_region(poly_name, mapset=poly_mapset, cat=attributes.index[i])
        valid_values = raster2numpy(RAST_MASK) == 1

        for j, raster in enumerate(rasters):
            # Save datetime only the first time
            if i == 0:
                dates[j] = raster['start_time']

            # Read raster data as array
            layer = raster2numpy(raster['name'], raster['mapset'])

            # Save statistic
            data[j, i] = FUN[stat](layer[valid_values])

    # Create a DataFrame
    print('\n\nCreating DataFrame...')
    df = pd.DataFrame(
        data,
        columns=attributes.values,
        index=dates
    )
    df.index.name = 'Dates'

    # Save file
    print('Creating delimited file {}...'.format(output))
    df.to_csv(output, sep=DELIMITERS[sep])

    # Remove polygon and raster mask
    gscript.run_command(
        'g.remove',
        flags='f',
        type='vector',
        name=POLY_MASK
    )
    gscript.run_command(
        'g.remove',
        flags='f',
        type='raster',
        name=RAST_MASK
    )


if __name__ == '__main__':
    sys.exit(main())
